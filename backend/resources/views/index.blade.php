<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-K4WRFWJ');</script>
    <!-- End Google Tag Manager -->

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if (Request::is('/'))
        <title>Liquidity Capital - For Growth</title>
        <meta name="description" content="Liquidity Capital is proud to introduce Rev-share Financing: providing growth capital and funding SaaS scaling, all while maintaining positive cash flow."/>
    @elseif(Request::is('solution'))
        <title>How Rev-Share Financing works</title>
        <meta name="description" content="Revenue-Share Financing was developed by a leading group of financial experts with a clear mission: deliver the best solution for tech companies' toughest financial challenges."/>
    @elseif(Request::is('cases'))
        <title>See who Liquidity has helped already</title>
        <meta name="description" content="We’ve already helped companies conquer their growth challenges. Here’s how we helped them meet their goals."/>
    @elseif(Request::is('team'))
        <title>Learn more about the Liquidity team</title>
        <meta name="description" content="Our team is comprised of Expert SaaS entrepreneurs, analysts, and financial experts. Learn more about them here."/>
    @elseif(Request::is('faq'))
        <title>Frequently-Asked Questions</title>
        <meta name="description" content="Have a question about our model? Maybe this will help."/>
    @elseif(Request::is('investors'))
        <title>לקראת פגישתנו הקרובה בנושא קרן ליקווידטי קפיטל</title>
    @else
        <title>Liquidity Capital - Rev-share Growth Financing</title>
    @endif
    <link href="/images/favicon.ico" rel="shortcut icon"/>
    <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel="stylesheet">
    <meta name="theme-color" content="#3C3F53" />
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    <style>
        .app-spinner {
            border: 4px solid #f3f3f3;
            border-top: 4px solid purple;
            border-right: 4px solid purple;
            animation: spin 1.5s linear infinite;
            flex-grow: 0;
            flex-shrink: 0;
            height: 8rem;
            width: 8rem;
            border-radius: 50%;
            margin-left: -2px;
        }
        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    {{--<script type="text/javascript"> //<![CDATA[--}}
    {{--var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");--}}
    {{--document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));--}}
    {{--//]]>--}}
    {{--</script>--}}

</head>
<body>


<div id="app" style="display: flex; align-items: center; justify-content: center; height: 100vh">
    <div class="app-spinner"></div>
</div>

<script src="{{ mix('js/app.js') }}"></script>

<script>
    let autocomplete;
    function initAutocomplete() {
        if (document.getElementById('autocomplete')) {
            autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));
        }
    }
</script>

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5-rCf_o25EBRE2S_cDsedaTM06YsJ4xs&libraries=places&callback=initAutocomplete&language=en">
</script>

<script src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages':['corechart']});
</script>
<!-- End of HubSpot Embed Code -->

<!-- <div class="comodo">
<script language="JavaScript" type="text/javascript">
    TrustLogo("http://liquidity-capital.com/comodo_secure_seal_76x26_transp.png", "CL1", "none");
</script>
</div> -->

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4WRFWJ"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

</body>
</html>
