# Base project

Docker running Nginx, Composer, Laravel, Vue.js MySQL.

## Install prerequisites

For now, this project has been mainly created for Unix `(Linux/MacOS)`. Perhaps it could work on Windows.

All requisites should be available for your distribution. The most important are :

* [Git](https://git-scm.com/downloads)
* [Docker](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install/)

Check if `docker-compose` is already installed by entering the following command :

```sh
which docker-compose
```

Check Docker Compose compatibility :

- [Compose file version 3 reference](https://docs.docker.com/compose/compose-file/)

## Run the application

1. Start the application :

    ```sh
    sudo docker-compose up -d
    ```
   **Please wait this might take a several minutes...**


2. Open [http://localhost](http://localhost/) your favorite browser.


3. Stop and clear services

    ```sh
    sudo docker-compose down -v
    ```